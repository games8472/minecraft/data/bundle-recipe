# Bundle Recipe

This is a datapack, that adds the recipe for the [bundle] that should be part of Caves & Cliffs Update.

The pack adds the recipe and the advancement to unlock the recipe.
The code is taken from original Mojang [CavesAndCliffs Preview-Datapack] from changlog of
the [21w19a snapshot].

So this pack should be fully compatible with the finial bundle implementation in the future.

## Install

### To new world
1. open minecraft, single player, new world, datapack,
2. download the file
3. drag and drop the file to the datapack screen or
4. open pack folder
5. drag and drop the file to the folder
6. click the arrow button in the datapack

### To existing world
1. go to your worlds-folder
2. download the file
3. drag and drop the file into the `datapack`-directory in your world
4. start your world


[CavesAndCliffs Preview-Datapack]: https://launcher.mojang.com/v1/objects/a6b56d6f14869646eb8d399e99a0149bdd954490/CavesAndCliffsPreview.zip
[21w19a snapshot]: https://www.minecraft.net/es-es/article/minecraft-snapshot-21w19a 
[bundle]: https://minecraft.fandom.com/wiki/Bundle